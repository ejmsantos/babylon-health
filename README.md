I concentrated my efforts on the architecture(MVVM) and tests side and decided to provide a simpler UI that can be easily improved.

### Frameworks:
  - Dagger
  - [Spek](https://blacklenspub.com/bacon-driven-development-in-kotlin-with-spek-376780e7ab3a) + junit5
  - Rx2
  - [Navigation component](https://developer.android.com/guide/navigation/navigation-getting-started)
  - [Android Architecture Components](https://developer.android.com/topic/libraries/architecture)

### Running tests
The tests run with the classic
```sh
$ ./gradlew testDebugUnitTest
```
command and successfully generate the report. But if you wish to run the tests on the IDE and get something like [this](tests.png) consider installing [this spek plugin](https://plugins.jetbrains.com/plugin/8564-spek/versions).

### Todos
  - Create styles and remove hardcoded dimens and text sizes in layouts
  - Move duplicated code in fragments to BaseFragment
  - Cleanup duplicated test data in Specs
  - Create local data sources(cached, persisted  or both)
  - Better error display treatment(e.g:snackbar with retry button)
  - Network "listener" to avoid requests when no network available
  - Test mappers

### Possible improvements / Features
  - Work on the UI / use better colour scheme
  - Show comments count and user info also in post list
  - Filter posts by user
  - Show more post detail information
  - Show more comments information
  - Use  Kotlin DSL instead of groovy