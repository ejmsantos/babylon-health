package com.eduardosantos.babylonhealth.di

import android.app.Application
import com.eduardosantos.babylonhealth.BabylonHealthApp
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import javax.inject.Singleton


@Singleton
@Component(modules = [AndroidInjectionModule::class,
    AppModule::class,
    MainActivityModule::class,
    TypicodeApiModule::class,
    NetworkModule::class,
    PostsModule::class])
interface AppComponent : AndroidInjector<DaggerApplication> {
    fun inject(app: BabylonHealthApp)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(app: Application): Builder

        fun build(): AppComponent
    }
}