package com.eduardosantos.babylonhealth.di

import android.app.Application
import android.content.Context
import android.content.res.Resources
import com.eduardosantos.babylonhealth.domain.AppSchedulerProvider
import com.eduardosantos.babylonhealth.domain.SchedulerProvider
import com.squareup.picasso.Picasso
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object AppModule {
    @Provides
    @JvmStatic
    @Singleton
    fun provideContext(app: Application): Context = app.applicationContext

    @Provides
    @JvmStatic
    @Singleton
    fun provideResources(app: Application): Resources = app.resources

    @Provides
    @JvmStatic
    @Singleton
    fun provideSchedulerProvider(): SchedulerProvider = AppSchedulerProvider()

    @Provides
    @JvmStatic
    @Singleton
    fun providePicasso(context: Context): Picasso = Picasso.Builder(context).build()
}