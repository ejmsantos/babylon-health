package com.eduardosantos.babylonhealth.di

import com.eduardosantos.babylonhealth.view.main.MainActivity
import com.eduardosantos.babylonhealth.view.postdetail.PostDetailFragment
import com.eduardosantos.babylonhealth.view.postdetail.PostDetailViewModel
import com.eduardosantos.babylonhealth.view.posts.PostsFragment
import com.eduardosantos.babylonhealth.view.posts.PostsViewModel
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector

@Module
internal abstract class MainActivityModule {
    @ActivityScope
    @ContributesAndroidInjector
    internal abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector
    internal abstract fun bindPostsFragment(): PostsFragment

    @ContributesAndroidInjector
    internal abstract fun bindPostDetailFragment(): PostDetailFragment

    @Module
    companion object {
        @JvmStatic
        @Provides
        fun providePostsViewModelFactory(viewModel: PostsViewModel): ViewModelProviderFactory<PostsViewModel> {
            return ViewModelProviderFactory(viewModel)
        }

        @JvmStatic
        @Provides
        fun providePostDetailViewModelFactory(viewModel: PostDetailViewModel): ViewModelProviderFactory<PostDetailViewModel> {
            return ViewModelProviderFactory(viewModel)
        }
    }
}