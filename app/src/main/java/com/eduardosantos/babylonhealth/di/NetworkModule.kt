package com.eduardosantos.babylonhealth.di

import android.content.Context
import android.net.ConnectivityManager
import com.eduardosantos.babylonhealth.BuildConfig
import com.google.gson.Gson
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides // *Not* a Singleton, so each API service gets a new instance
    fun provideRetrofitBuilder(
        rxJavaCallAdapterFactory: RxJava2CallAdapterFactory
    ): Retrofit.Builder {
        return Retrofit.Builder()
            .addCallAdapterFactory(rxJavaCallAdapterFactory)
    }

    @Provides // *Not* a Singleton, so each API service gets a new instance
    fun provideHttpBuilder(): OkHttpClient.Builder {
        val builder = OkHttpClient.Builder()

        if (BuildConfig.DEBUG) {
            val httpLoggingInterceptor = HttpLoggingInterceptor()
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            builder.addInterceptor(httpLoggingInterceptor)
        }

        return builder
    }

    @Module
    companion object {
        @Provides
        @Singleton
        @JvmStatic
        fun provideGson(): Gson = Gson()

        @Provides
        @Singleton
        @JvmStatic
        fun provideGsonConverterFactory(): retrofit2.Converter.Factory = GsonConverterFactory.create()

        @Provides
        @Singleton
        @JvmStatic
        fun provideRxJavaCallAdapter(): RxJava2CallAdapterFactory = RxJava2CallAdapterFactory.create()

        @Provides
        @Singleton
        @JvmStatic
        fun provideConnectivityManager(context: Context): ConnectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    }
}