package com.eduardosantos.babylonhealth.di

import android.content.res.Resources
import com.eduardosantos.babylonhealth.domain.posts.PostsRepository
import com.eduardosantos.babylonhealth.domain.posts.PostsRepositoryType
import com.eduardosantos.babylonhealth.domain.posts.PostsUseCase
import com.eduardosantos.babylonhealth.domain.posts.PostsUseCaseType
import com.eduardosantos.babylonhealth.domain.posts.data.mapper.CommentsMapper
import com.eduardosantos.babylonhealth.domain.posts.data.mapper.PostsMapper
import com.eduardosantos.babylonhealth.domain.posts.data.remote.PostsDataRemoteSource
import com.eduardosantos.babylonhealth.domain.posts.data.remote.PostsDataRemoteSourceType
import com.eduardosantos.babylonhealth.domain.posts.data.remote.retrofit.TypicodeApiService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object PostsModule {

    @Provides
    @Singleton
    @JvmStatic
    fun providePostsUseCase(
        postsRepository: PostsRepositoryType
    ): PostsUseCaseType =
        PostsUseCase(postsRepository)

    @Provides
    @Singleton
    @JvmStatic
    fun providePostsRepository(
        postsDataRemoteSource: PostsDataRemoteSourceType,
        postsMapper: PostsMapper,
        commentsMapper: CommentsMapper,
        resources: Resources
    ): PostsRepositoryType =
        PostsRepository(postsDataRemoteSource, postsMapper, commentsMapper, resources)

    @Provides
    @Singleton
    @JvmStatic
    fun providePostsMapper(): PostsMapper =
        PostsMapper()

    @Provides
    @Singleton
    @JvmStatic
    fun provideCommentsMapper(): CommentsMapper =
        CommentsMapper()

    @Provides
    @Singleton
    @JvmStatic
    fun providePostsDataRemoteSource(
        typicodeApiService: TypicodeApiService
    ): PostsDataRemoteSourceType =
        PostsDataRemoteSource(typicodeApiService)

}