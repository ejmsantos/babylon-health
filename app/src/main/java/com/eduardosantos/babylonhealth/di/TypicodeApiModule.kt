package com.eduardosantos.babylonhealth.di

import android.content.res.Resources
import com.eduardosantos.babylonhealth.R
import com.eduardosantos.babylonhealth.domain.posts.data.remote.retrofit.TypicodeApiService
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import javax.inject.Named
import javax.inject.Singleton

@Module
object  TypicodeApiModule {

    private const val TYPICODE_API = "TYPICODE_API"
    private const val TYPICODE_BASE_URL = "TYPICODE_BASE_URL"

    @Provides
    @JvmStatic
    @Singleton
    internal fun provideTypiCodeApiService(@Named(TYPICODE_API) retrofit: Retrofit): TypicodeApiService {
        return retrofit.create(TypicodeApiService::class.java)
    }

    @Provides
    @JvmStatic
    @Named(TYPICODE_API)
    internal fun provideRetrofit(
        httpBuilder: OkHttpClient.Builder,
        retrofitBuilder: Retrofit.Builder,
        @Named(TYPICODE_BASE_URL) baseUrl: String,
        converter: retrofit2.Converter.Factory
    ): Retrofit {
        return retrofitBuilder
            .client(httpBuilder.build())
            .baseUrl(baseUrl)
            .addConverterFactory(converter)
            .build()
    }

    @Provides
    @JvmStatic
    @Named(TYPICODE_BASE_URL)
    internal fun provideBaseUrl(resources: Resources): String {
        return resources.getString(R.string.typicode_base_url)
    }
}