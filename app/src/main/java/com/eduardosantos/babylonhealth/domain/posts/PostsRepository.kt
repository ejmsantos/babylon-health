package com.eduardosantos.babylonhealth.domain.posts

import android.content.res.Resources
import com.eduardosantos.babylonhealth.R
import com.eduardosantos.babylonhealth.domain.posts.data.local.model.PostDetailUIModel
import com.eduardosantos.babylonhealth.domain.posts.data.local.model.PostUIModel
import com.eduardosantos.babylonhealth.domain.posts.data.mapper.CommentsMapper
import com.eduardosantos.babylonhealth.domain.posts.data.mapper.PostsMapper
import com.eduardosantos.babylonhealth.domain.posts.data.remote.PostsDataRemoteSourceType
import com.eduardosantos.babylonhealth.domain.posts.data.remote.model.CommentApiModel
import com.eduardosantos.babylonhealth.domain.posts.data.remote.model.UserApiModel
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import javax.inject.Inject

interface PostsRepositoryType {
    fun getPosts(): Single<List<PostUIModel>>
    fun getPostDetail(userId: Int, postId: Int): Single<PostDetailUIModel>
}

class PostsRepository @Inject constructor(
    private val postsDataRemoteSourceType: PostsDataRemoteSourceType,
    private val postsMapper: PostsMapper,
    private val commentsMapper: CommentsMapper,
    private val resources: Resources
) : PostsRepositoryType {
    override fun getPosts(): Single<List<PostUIModel>> {
        //todo local data source
        return postsDataRemoteSourceType.getPosts()
            .map { postsMapper.map(it) }
    }

    override fun getPostDetail(userId: Int, postId: Int): Single<PostDetailUIModel> {
        //todo local data source
        return postsDataRemoteSourceType.getUser(userId).zipWith(postsDataRemoteSourceType.getComments(postId),
            BiFunction { userApiModel: List<UserApiModel>, comments: List<CommentApiModel> ->
                val username = userApiModel[0].name
                PostDetailUIModel(
                    username = username,
                    avatarURL = resources.getString(R.string.adorable_base_url, username.hashCode().toString()),
                    comments = commentsMapper.map(comments)
                )
            })
    }
}