package com.eduardosantos.babylonhealth.domain.posts

import com.eduardosantos.babylonhealth.domain.posts.data.local.model.PostDetailUIModel
import com.eduardosantos.babylonhealth.domain.posts.data.local.model.PostUIModel
import io.reactivex.Single
import javax.inject.Inject

interface PostsUseCaseType {
    fun getPosts(): Single<List<PostUIModel>>
    fun getPostDetail(postUIModel: PostUIModel): Single<PostDetailUIModel>
}

class PostsUseCase @Inject constructor(
    private val postsRepository: PostsRepositoryType
) : PostsUseCaseType {
    override fun getPosts(): Single<List<PostUIModel>> {
        return postsRepository.getPosts()
    }

    override fun getPostDetail(postUIModel: PostUIModel): Single<PostDetailUIModel> {
        return postsRepository.getPostDetail(postUIModel.userId, postUIModel.id)
    }
}