package com.eduardosantos.babylonhealth.domain.posts.data.local.model

data class PostDetailUIModel(
    val username: String,
    val avatarURL: String,
    val comments:  List<CommentUIModel>
)

data class CommentUIModel(
    val email: String,
    val comment: String
)

