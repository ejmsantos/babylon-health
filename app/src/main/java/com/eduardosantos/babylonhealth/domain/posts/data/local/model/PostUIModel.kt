package com.eduardosantos.babylonhealth.domain.posts.data.local.model

import android.os.Parcel
import android.os.Parcelable

data class PostUIModel(
    val body: String,
    val id: Int = 0,
    val title: String,
    val userId: Int = 0
):Parcelable {
    constructor(parcel: Parcel) : this(
        body = parcel.readString()!!,
        id = parcel.readInt(),
        title = parcel.readString()!!,
        userId = parcel.readInt()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(body)
        parcel.writeInt(id)
        parcel.writeString(title)
        parcel.writeInt(userId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PostUIModel> {
        override fun createFromParcel(parcel: Parcel): PostUIModel {
            return PostUIModel(parcel)
        }

        override fun newArray(size: Int): Array<PostUIModel?> {
            return arrayOfNulls(size)
        }
    }

}