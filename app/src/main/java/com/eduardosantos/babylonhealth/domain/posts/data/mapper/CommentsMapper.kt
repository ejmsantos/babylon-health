package com.eduardosantos.babylonhealth.domain.posts.data.mapper

import com.eduardosantos.babylonhealth.domain.Mapper
import com.eduardosantos.babylonhealth.domain.posts.data.local.model.CommentUIModel
import com.eduardosantos.babylonhealth.domain.posts.data.remote.model.CommentApiModel

class CommentsMapper : Mapper<CommentApiModel, CommentUIModel>() {
    override fun map(from: CommentApiModel) =
        CommentUIModel(
            email = from.email,
            comment = from.body
        )
}