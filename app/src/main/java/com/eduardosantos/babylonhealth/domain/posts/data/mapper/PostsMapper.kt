package com.eduardosantos.babylonhealth.domain.posts.data.mapper

import com.eduardosantos.babylonhealth.domain.Mapper
import com.eduardosantos.babylonhealth.domain.posts.data.local.model.PostUIModel
import com.eduardosantos.babylonhealth.domain.posts.data.remote.model.PostApiModel

class PostsMapper : Mapper<PostApiModel, PostUIModel>() {
    override fun map(from: PostApiModel) =
        PostUIModel(
            from.body.capitalize(),
            from.id,
            from.title.capitalize(),
            from.userId
        )
}