package com.eduardosantos.babylonhealth.domain.posts.data.remote

import com.eduardosantos.babylonhealth.domain.posts.data.remote.model.CommentApiModel
import com.eduardosantos.babylonhealth.domain.posts.data.remote.model.PostApiModel
import com.eduardosantos.babylonhealth.domain.posts.data.remote.model.UserApiModel
import com.eduardosantos.babylonhealth.domain.posts.data.remote.retrofit.TypicodeApiService
import io.reactivex.Single
import javax.inject.Inject

interface PostsDataRemoteSourceType {
    fun getPosts(): Single<List<PostApiModel>>
    fun getUser(userId: Int): Single<List<UserApiModel>>
    fun getComments(postId: Int): Single<List<CommentApiModel>>
}

class PostsDataRemoteSource @Inject constructor(
    private val typicodeApiService: TypicodeApiService
) : PostsDataRemoteSourceType {
    override fun getPosts(): Single<List<PostApiModel>> {
        return typicodeApiService.getPosts()
    }

    override fun getUser(userId: Int): Single<List<UserApiModel>> {
        return typicodeApiService.getUsers(userId)
    }

    override fun getComments(postId: Int): Single<List<CommentApiModel>> {
        return typicodeApiService.getComments(postId)
    }
}