package com.eduardosantos.babylonhealth.domain.posts.data.remote.model

data class CommentApiModel(
    val body: String,
    val email: String,
    val id: Int,
    val name: String,
    val postId: Int
)