package com.eduardosantos.babylonhealth.domain.posts.data.remote.model

data class PostApiModel(
    val body: String,
    val id: Int,
    val title: String,
    val userId: Int
)