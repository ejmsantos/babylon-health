package com.eduardosantos.babylonhealth.domain.posts.data.remote.retrofit

import com.eduardosantos.babylonhealth.domain.posts.data.remote.model.CommentApiModel
import com.eduardosantos.babylonhealth.domain.posts.data.remote.model.PostApiModel
import com.eduardosantos.babylonhealth.domain.posts.data.remote.model.UserApiModel
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface TypicodeApiService {

    @GET("posts")
    fun getPosts(): Single<List<PostApiModel>>

    @GET("users")
    fun getUsers(
        @Query("id") userId: Int
    ): Single<List<UserApiModel>>

    @GET("comments")
    fun getComments(
        @Query("postId") userId: Int
    ): Single<List<CommentApiModel>>


}
