package com.eduardosantos.babylonhealth.view.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.eduardosantos.babylonhealth.di.ViewModelProviderFactory
import com.eduardosantos.babylonhealth.domain.SchedulerProvider
import dagger.android.support.DaggerFragment
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

abstract class BaseFragment<T : BaseViewModel, B : ViewDataBinding> : DaggerFragment(),
    BaseView<T> {
    @Inject
    lateinit var schedulers: SchedulerProvider

    @Inject
    protected lateinit var viewModelProviderFactory: ViewModelProviderFactory<T>

    protected val subscriptions = CompositeDisposable()
    protected lateinit var binding: B

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, bindingLayout, container, false)
        return binding.root
    }

    override fun onPause() {
        subscriptions.clear()
        super.onPause()
    }
}


