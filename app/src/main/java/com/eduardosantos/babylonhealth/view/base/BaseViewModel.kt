package com.eduardosantos.babylonhealth.view.base

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.eduardosantos.babylonhealth.domain.SchedulerProvider
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import javax.inject.Inject

interface BaseViewModelInputs

interface BaseViewModelOutputs {
    fun error(): Observable<String>
    fun finish(): Observable<Unit>
    fun refreshing(): Observable<Boolean>
}

open class BaseViewModel(application: Application) : AndroidViewModel(application),
    BaseViewModelInputs,
    BaseViewModelOutputs {

    @Inject
    lateinit var schedulerProvider: SchedulerProvider

    protected val subscriptions = CompositeDisposable()

    open val inputs: BaseViewModelInputs
        get() = this

    open val outputs: BaseViewModelOutputs
        get() = this

    protected val finish: Subject<Unit> = PublishSubject.create<Unit>()
    protected val error: Subject<String> = PublishSubject.create<String>()
    protected val refreshing: Subject<Boolean> = BehaviorSubject.createDefault(false)

    override fun onCleared() {
        super.onCleared()
        subscriptions.clear()
    }

    override fun error(): Observable<String> {
        return error.observeOn(schedulerProvider.ui())
            .hide()
    }

    override fun finish(): Observable<Unit> {
        return finish.observeOn(schedulerProvider.ui())
            .hide()
    }

    override fun refreshing(): Observable<Boolean> {
        return refreshing.observeOn(schedulerProvider.ui())
            .hide()
    }
}
