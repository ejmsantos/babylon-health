package com.eduardosantos.babylonhealth.view.base

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView

interface BindableAdapter<T> {
    fun setData(data: T)
}

@BindingAdapter("data")
fun <T> setRecyclerViewProperties(recyclerView: RecyclerView, data: T) {
    if (recyclerView.adapter is BindableAdapter<*>) {
        (recyclerView.adapter as BindableAdapter<T>).setData(data)
    }
}

@BindingAdapter("dividerDirection")
fun setItemDecoration(view: RecyclerView, oldDirection: Int, newDirection: Int) {
    if (oldDirection != newDirection) {
        val decoration = DividerItemDecoration(view.context, newDirection)
        view.addItemDecoration(decoration)
    }
}