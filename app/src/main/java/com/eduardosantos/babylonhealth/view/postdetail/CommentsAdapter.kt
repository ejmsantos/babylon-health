package com.eduardosantos.babylonhealth.view.postdetail

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.eduardosantos.babylonhealth.R
import com.eduardosantos.babylonhealth.domain.posts.data.local.model.CommentUIModel
import com.eduardosantos.babylonhealth.view.base.BindableAdapter
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.comment_item.view.*

class CommentsAdapter : RecyclerView.Adapter<CommentsAdapter.CommentHolder>(), BindableAdapter<List<CommentUIModel>> {

    private val subscriptions = CompositeDisposable()
    var comments = emptyList<CommentUIModel>()

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        subscriptions.clear()
    }

    override fun setData(data: List<CommentUIModel>) {
        comments = data
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentHolder {
        val inflater = LayoutInflater.from(parent.context)
        return CommentHolder(inflater.inflate(R.layout.comment_item, parent, false))
    }

    override fun getItemCount() = comments.size

    override fun onBindViewHolder(holder: CommentHolder, position: Int) {
        holder.bind(comments[position])
    }

    class CommentHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(comment: CommentUIModel) {
                itemView.commentView.text = "- ${comment.email}: ${comment.comment}"
        }
    }
}
