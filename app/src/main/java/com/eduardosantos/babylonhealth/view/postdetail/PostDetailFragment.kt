package com.eduardosantos.babylonhealth.view.postdetail

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.navArgs
import com.eduardosantos.babylonhealth.BR
import com.eduardosantos.babylonhealth.R
import com.eduardosantos.babylonhealth.databinding.FragmentPostDetailBinding
import com.eduardosantos.babylonhealth.domain.posts.data.local.model.PostUIModel
import com.eduardosantos.babylonhealth.view.base.BaseFragment
import com.squareup.picasso.Picasso

class PostDetailFragment : BaseFragment<PostDetailViewModel, FragmentPostDetailBinding>(){
    private val args: PostDetailFragmentArgs by navArgs()

    private val commentsAdapter = CommentsAdapter()

    private lateinit var postUIModel: PostUIModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupData()
    }

    override fun onResume() {
        super.onResume()
        setupInputs()
        setupOutputs()
    }

    private fun setupData() {
        postUIModel = args.postUIModel
        binding.setVariable(BR.postUIModel, postUIModel)
        viewModel.onLoadPostDetail(postUIModel)
    }

    private fun setupOutputs() {
        viewModel.outputs.getPostDetail().subscribe {
            Picasso.get().load(it.avatarURL)
                .placeholder(R.drawable.ic_account_box_black_24dp)
                .into(binding.userAvatarView)

            binding.setVariable(BR.postDetailUIModel, it)
            binding.recyclerView.adapter = commentsAdapter
            binding.commentsCountView.text =
                resources.getString(R.string.comments_count_text, it.comments.size)
        }.also { subscriptions.add(it) }


        viewModel.outputs.refreshing()
            .subscribe {
                if (it) {
                    binding.recyclerView.visibility = View.GONE
                    binding.refreshView.isRefreshing = true
                } else {
                    binding.recyclerView.visibility = View.VISIBLE
                    binding.refreshView.isRefreshing = false
                }
            }.also { subscriptions.add(it) }
    }

    private fun setupInputs() {
        binding.refreshView.setOnRefreshListener {
            viewModel.inputs.onLoadPostDetail(postUIModel)
        }
    }

    override val bindingLayout: Int = R.layout.fragment_post_detail
    override val viewModel: PostDetailViewModel by lazy {
        ViewModelProviders.of(this, viewModelProviderFactory).get(PostDetailViewModel::class.java)
    }
}