package com.eduardosantos.babylonhealth.view.postdetail

import android.app.Application
import com.eduardosantos.babylonhealth.domain.posts.PostsUseCaseType
import com.eduardosantos.babylonhealth.domain.posts.data.local.model.PostDetailUIModel
import com.eduardosantos.babylonhealth.domain.posts.data.local.model.PostUIModel
import com.eduardosantos.babylonhealth.view.base.BaseViewModel
import com.eduardosantos.babylonhealth.view.base.BaseViewModelInputs
import com.eduardosantos.babylonhealth.view.base.BaseViewModelOutputs
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import javax.inject.Inject

interface PostDetailViewModelInputs: BaseViewModelInputs {
    fun onLoadPostDetail(postdetailUIModel: PostUIModel)
}

interface PostDetailViewModelOutputs: BaseViewModelOutputs {
    fun getPostDetail() : Observable<PostDetailUIModel>
}

class PostDetailViewModel @Inject constructor(application: Application)
    : BaseViewModel(application), PostDetailViewModelInputs, PostDetailViewModelOutputs {
    override val inputs: PostDetailViewModelInputs
        get() = this

    override val outputs: PostDetailViewModelOutputs
        get() = this

    @Inject
    lateinit var postsUseCase: PostsUseCaseType

    private val getPostDetail = PublishSubject.create<PostDetailUIModel>()

    override fun onLoadPostDetail(postdetailUIModel: PostUIModel) {
        postsUseCase.getPostDetail(postdetailUIModel)
            .doOnSubscribe { refreshing.onNext(true) }
            .doFinally { refreshing.onNext(false) }
            .compose(schedulerProvider.doOnIoObserveOnMainSingle())
            .subscribe({
                Timber.e("Post detail ${postdetailUIModel.id} successfully fetched")
                getPostDetail.onNext(it)
            },{
                val errorText = "Error loading post detail: ${it.message}"
                Timber.e(errorText)
                error.onNext(errorText)
            }).also { subscriptions.add(it) }
    }

    override fun getPostDetail(): Observable<PostDetailUIModel> {
        return getPostDetail.observeOn(schedulerProvider.ui()).hide()
    }

}