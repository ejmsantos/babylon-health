package com.eduardosantos.babylonhealth.view.posts

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.eduardosantos.babylonhealth.R
import com.eduardosantos.babylonhealth.domain.posts.data.local.model.PostUIModel
import com.eduardosantos.babylonhealth.view.base.BindableAdapter
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.post_item.view.*
import java.util.concurrent.TimeUnit

class PostsAdapter : RecyclerView.Adapter<PostsAdapter.PostHolder>(), BindableAdapter<List<PostUIModel>> {

    private val subscriptions = CompositeDisposable()
    val itemClick = PublishSubject.create<Int>()
    var posts = emptyList<PostUIModel>()

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        subscriptions.clear()
    }

    override fun setData(data: List<PostUIModel>) {
        posts = data
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostHolder {
        val inflater = LayoutInflater.from(parent.context)
        return PostHolder(inflater.inflate(R.layout.post_item, parent, false))
    }

    override fun getItemCount() = posts.size

    override fun onBindViewHolder(holder: PostHolder, position: Int) {
        bindClickAction(holder.itemView) {
            itemClick.onNext(position)
        }
        holder.bind(posts[position])
    }

    class PostHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(post: PostUIModel) {
            itemView.titleView.text = "${post.id}. ${post.title}"
        }
    }

    private fun bindClickAction(view: View, clickAction: () -> Unit) {
        RxView.clicks(view)
            .throttleFirst(BUTTON_DEBOUNCE_TIMEOUT_MS, TimeUnit.MILLISECONDS)
            .subscribe { clickAction() }
            .also { subscriptions.add(it) }
    }

    companion object {
        const val BUTTON_DEBOUNCE_TIMEOUT_MS = 500L
    }
}
