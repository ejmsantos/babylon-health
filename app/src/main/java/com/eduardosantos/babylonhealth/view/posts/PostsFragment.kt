package com.eduardosantos.babylonhealth.view.posts

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.eduardosantos.babylonhealth.BR
import com.eduardosantos.babylonhealth.R
import com.eduardosantos.babylonhealth.databinding.FragmentPostsBinding
import com.eduardosantos.babylonhealth.domain.posts.data.local.model.PostUIModel
import com.eduardosantos.babylonhealth.view.base.BaseFragment


class PostsFragment : BaseFragment<PostsViewModel, FragmentPostsBinding>(){

    private val postsAdapter = PostsAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.inputs.onLoadPosts()
    }

    override fun onResume() {
        super.onResume()
        setupInputs()
        setupOutputs()
    }

    private fun setupOutputs() {
        viewModel.outputs.getPosts()
            .subscribe {
                binding.placeHolderText.visibility = View.GONE
                binding.setVariable(BR.posts, it)
                binding.recyclerView.adapter = postsAdapter
            }.also { subscriptions.add(it) }

        viewModel.outputs.refreshing()
            .subscribe {
                if (it) {
                    binding.recyclerView.visibility = View.GONE
                    binding.refreshView.isRefreshing = true
                } else {
                    binding.recyclerView.visibility = View.VISIBLE
                    binding.refreshView.isRefreshing = false
                }
            }.also { subscriptions.add(it) }

        viewModel.outputs.error()
            .subscribe {
                binding.recyclerView.visibility = View.GONE
                binding.refreshView.isRefreshing = false
                binding.placeHolderText.visibility = View.VISIBLE
                binding.placeHolderText.text = it
            }.also { subscriptions.add(it) }

        viewModel.outputs.navigateToPost().subscribe {
            navigateToPost(it)
        }.also { subscriptions.add(it) }
    }

    private fun setupInputs() {
        binding.refreshView.setOnRefreshListener {
            viewModel.inputs.onLoadPosts()
        }

        postsAdapter.itemClick.subscribe {
            viewModel.inputs.onPostClicked(it)
        }.also { subscriptions.add(it) }
    }

    private fun navigateToPost(postUIModel: PostUIModel) {
        val action = PostsFragmentDirections.goToPostDetailFromPosts(postUIModel)
        findNavController().navigate(action)
    }

    override val bindingLayout: Int = R.layout.fragment_posts
    override val viewModel: PostsViewModel by lazy {
        ViewModelProviders.of(this, viewModelProviderFactory).get(PostsViewModel::class.java)
    }
}