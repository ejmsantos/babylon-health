package com.eduardosantos.babylonhealth.view.posts

import android.app.Application
import com.eduardosantos.babylonhealth.domain.posts.PostsUseCaseType
import com.eduardosantos.babylonhealth.domain.posts.data.local.model.PostUIModel
import com.eduardosantos.babylonhealth.view.base.BaseViewModel
import com.eduardosantos.babylonhealth.view.base.BaseViewModelInputs
import com.eduardosantos.babylonhealth.view.base.BaseViewModelOutputs
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import javax.inject.Inject

interface PostsViewModelInputs: BaseViewModelInputs {
    fun onLoadPosts()
    fun onPostClicked(position: Int)
}

interface PostsViewModelOutputs: BaseViewModelOutputs {
    fun getPosts(): Observable<List<PostUIModel>>
    fun navigateToPost(): Observable<PostUIModel>
}

class PostsViewModel @Inject constructor(application: Application)
    : BaseViewModel(application), PostsViewModelInputs, PostsViewModelOutputs {

    override val inputs: PostsViewModelInputs
        get() = this

    override val outputs: PostsViewModelOutputs
        get() = this

    private val getPosts = PublishSubject.create<List<PostUIModel>>()
    private val navigateToPost = PublishSubject.create<PostUIModel>()

    @Inject
    lateinit var postsUseCase: PostsUseCaseType

    lateinit var posts : List<PostUIModel>

    override fun onLoadPosts() {
        postsUseCase.getPosts()
            .doOnSubscribe { refreshing.onNext(true) }
            .doFinally { refreshing.onNext(false) }
            .compose(schedulerProvider.doOnIoObserveOnMainSingle())
            .subscribe({
                Timber.e("Posts successfully fetched")
                posts = it
                getPosts.onNext(it)
            },{
                val errorText = "Error loading posts: ${it.message}"
                Timber.e(errorText)
                error.onNext(errorText)
            }).also { subscriptions.add(it) }
    }

    override fun onPostClicked(position: Int) {
        navigateToPost.onNext(posts[position])
    }

    override fun getPosts(): Observable<List<PostUIModel>> {
        return getPosts.observeOn(schedulerProvider.ui()).hide()
    }

    override fun navigateToPost(): Observable<PostUIModel> {
        return navigateToPost.observeOn(schedulerProvider.ui()).hide()
    }
}