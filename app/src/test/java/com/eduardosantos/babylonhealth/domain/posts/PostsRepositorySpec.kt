package com.eduardosantos.babylonhealth.domain.posts

import android.content.res.Resources
import com.eduardosantos.babylonhealth.domain.posts.data.mapper.CommentsMapper
import com.eduardosantos.babylonhealth.domain.posts.data.mapper.PostsMapper
import com.eduardosantos.babylonhealth.domain.posts.data.remote.PostsDataRemoteSourceType
import com.eduardosantos.babylonhealth.domain.posts.data.remote.model.*
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.Single
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import org.mockito.Mockito

class PostsRepositorySpec : Spek({
    val userId = 1
    val postId = 1
    val testPostApiModelList = listOf(
        PostApiModel(
            "body",
            1,
            "title",
            1
        )
    )
    val testUser = listOf(
        UserApiModel(
            name = "",
            id = userId,
            address = Address(
                "",
                Geo("", ""), "", "", ""
            ),
            email = "",
            username = "",
            company = Company("", "", ""),
            phone = "",
            website = ""
        )
    )
    val testComments = listOf(
        CommentApiModel(
            "",
            "",
            1,
            "",
            postId
        )
    )

    val postsDataRemoteSourceMock = Mockito.mock(PostsDataRemoteSourceType::class.java).apply {
        Mockito.`when`(getPosts()).thenReturn(Single.just(testPostApiModelList))
        Mockito.`when`(getUser(userId)).thenReturn(Single.just(testUser))
        Mockito.`when`(getComments(postId)).thenReturn(Single.just(testComments))
    }
    val postsMapperMock = Mockito.mock(PostsMapper::class.java)
    val commentsMapperMock = Mockito.mock(CommentsMapper::class.java)
    val resourcesMock = Mockito.mock(Resources::class.java).apply {
        Mockito.`when`(getString(any(), any())).thenReturn("any string")
    }

    val postsRepository = PostsRepository(postsDataRemoteSourceMock, postsMapperMock, commentsMapperMock, resourcesMock)

    describe("getPosts") {
        on("posts requested") {
            val sub = postsRepository.getPosts()
            it("requests posts to PostsDataRemoteSourceType") {
                verify(postsDataRemoteSourceMock).getPosts()
            }

            val testObserver = sub.test()
            it("maps posts server response") {
                verify(postsMapperMock).map(testPostApiModelList)
            }

            it("emits posts") {
                testObserver.assertComplete()
                testObserver.assertNoErrors()
            }
        }
    }

    describe("getPostDetail") {
        on("post detail userId:$userId postId:$postId requested") {
            val sub = postsRepository.getPostDetail(userId, postId)
            it("requests user  id:$userId to PostsDataRemoteSourceType") {
                verify(postsDataRemoteSourceMock).getUser(userId)
            }

            it("requests comments  postId:$postId to PostsDataRemoteSourceType") {
                verify(postsDataRemoteSourceMock).getComments(postId)
            }

            val testObserver = sub.test()
            it("maps comments server response") {
                verify(commentsMapperMock).map(testComments)
            }

            it("emits post detail") {
                testObserver.assertComplete()
                testObserver.assertNoErrors()
            }
        }
    }

})