package com.eduardosantos.babylonhealth.domain.posts

import com.eduardosantos.babylonhealth.domain.posts.data.local.model.PostUIModel
import com.nhaarman.mockito_kotlin.verify
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import org.mockito.Mockito

class PostsUseCaseSpec: Spek({
    val userId = 1
    val postId = 1
    val testPostUIModel =
        PostUIModel("", postId, "", userId)
    val  postsRepositoryMock = Mockito.mock(PostsRepositoryType::class.java)

    val postsUseCase = PostsUseCase(postsRepositoryMock)

    describe("getPosts") {
        on("posts requested") {
            postsUseCase.getPosts()
            it("requests posts to PostsRepositoryType") {
                verify(postsRepositoryMock).getPosts()
            }
        }
    }

    describe("getPostDetail") {
        on("post detail") {
            postsUseCase.getPostDetail(testPostUIModel)
            it("requests post detail postId:$postId userId:$userId to PostsRepositoryType") {
                verify(postsRepositoryMock).getPostDetail(userId, postId)
            }
        }
    }
})