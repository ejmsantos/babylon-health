package com.eduardosantos.babylonhealth.domain.posts.data

import com.eduardosantos.babylonhealth.domain.posts.data.remote.PostsDataRemoteSource
import com.eduardosantos.babylonhealth.domain.posts.data.remote.retrofit.TypicodeApiService
import com.nhaarman.mockito_kotlin.verify
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import org.mockito.Mockito

class PostsDataRemoteSourceSpec: Spek({
    val  typicodeApiServiceMock = Mockito.mock(TypicodeApiService::class.java)

    val postsDataRemoteSource = PostsDataRemoteSource(typicodeApiServiceMock)

    describe("getPosts") {
        on("posts requested") {
            postsDataRemoteSource.getPosts()
            it("requests posts to TypicodeApiService") {
                verify(typicodeApiServiceMock).getPosts()
            }
        }
    }

    describe("getUser") {
        val userId = 1
        on("user id:$userId requested") {
            postsDataRemoteSource.getUser(userId)
            it("requests user id:$userId to TypicodeApiService") {
                verify(typicodeApiServiceMock).getUsers(userId)
            }
        }
    }

    describe("getComments") {
        val postId = 1
        on("comments postId:$postId requested") {
            postsDataRemoteSource.getComments(postId)
            it("requests comments postId:$postId to TypicodeApiService") {
                verify(typicodeApiServiceMock).getComments(postId)
            }
        }
    }
})