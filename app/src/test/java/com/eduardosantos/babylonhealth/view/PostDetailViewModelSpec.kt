package com.eduardosantos.babylonhealth.view

import com.eduardosantos.babylonhealth.BabylonHealthApp
import com.eduardosantos.babylonhealth.domain.TestSchedulerProvider
import com.eduardosantos.babylonhealth.domain.posts.PostsUseCaseType
import com.eduardosantos.babylonhealth.domain.posts.data.local.model.PostDetailUIModel
import com.eduardosantos.babylonhealth.domain.posts.data.local.model.PostUIModel
import com.eduardosantos.babylonhealth.view.postdetail.PostDetailViewModel
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import org.mockito.Mockito

class PostDetailViewModelSpec : Spek({
    val testPost = PostUIModel("body", 1, "title", 1)
    val testPostDetail =
        PostDetailUIModel("", "", listOf())

    val postsUseCaseMock = Mockito.mock(PostsUseCaseType::class.java)
    val applicationMock = Mockito.mock(BabylonHealthApp::class.java)

    val postsViewModel = PostDetailViewModel(applicationMock).apply {
        schedulerProvider = TestSchedulerProvider()
        postsUseCase = postsUseCaseMock
    }

    val postsViewModelSpy = Mockito.spy(postsViewModel)

    lateinit var refreshTestSub: TestObserver<Boolean>
    lateinit var getPostDetailTestSub: TestObserver<PostDetailUIModel>
    lateinit var errorTestSub: TestObserver<String>

    describe("onLoadPostDetail") {
        beforeEachTest {
            Mockito.clearInvocations(postsViewModelSpy)
            refreshTestSub = postsViewModelSpy.outputs.refreshing().test()
            getPostDetailTestSub = postsViewModelSpy.outputs.getPostDetail().test()
            errorTestSub = postsViewModelSpy.outputs.error().test()
        }

        on("post detail data successfully fetched") {
            postsUseCaseMock.apply {
                Mockito.`when`(getPostDetail(testPost))
                    .thenReturn(Single.just(testPostDetail))
            }

            postsViewModelSpy.onLoadPostDetail(testPost)

            it("emits refreshing start and end") {
                refreshTestSub.assertValueAt(1, true)
                refreshTestSub.assertValueAt(2, false)
            }

            it("emits no errors") {
                errorTestSub.assertNoValues()
            }

            it("emits post detail") {
                getPostDetailTestSub.assertValue(testPostDetail)
            }

        }

        on("error fetching post detail data") {
            postsUseCaseMock.apply {
                Mockito.`when`(getPostDetail(testPost))
                    .thenReturn(Single.error(Throwable("error")))
            }

            postsViewModelSpy.onLoadPostDetail(testPost)

            it("emits refreshing start and end") {
                refreshTestSub.assertValueAt(1, true)
                refreshTestSub.assertValueAt(2, false)
            }

            it("emits error") {
                errorTestSub.assertValue("Error loading post detail: error")
            }

            it("emits no post detail") {
                getPostDetailTestSub.assertNoValues()
            }

        }
    }

})