package com.eduardosantos.babylonhealth.view

import com.eduardosantos.babylonhealth.BabylonHealthApp
import com.eduardosantos.babylonhealth.domain.TestSchedulerProvider
import com.eduardosantos.babylonhealth.domain.posts.PostsUseCaseType
import com.eduardosantos.babylonhealth.domain.posts.data.local.model.PostUIModel
import com.eduardosantos.babylonhealth.view.posts.PostsViewModel
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import org.mockito.Mockito

class PostsViewModelSpec : Spek({
    val testPosition = 0
    val testPost = PostUIModel("body", 0, "title", 0)
    val testPostsList = listOf(testPost)

    val postsUseCaseMock = Mockito.mock(PostsUseCaseType::class.java)
    val applicationMock = Mockito.mock(BabylonHealthApp::class.java)

    val postsViewModel = PostsViewModel(applicationMock).apply {
        schedulerProvider = TestSchedulerProvider()
        postsUseCase = postsUseCaseMock
        posts = testPostsList
    }

    val postsViewModelSpy = Mockito.spy(postsViewModel)

    lateinit var refreshTestSub: TestObserver<Boolean>
    lateinit var getPostsTestSub: TestObserver<List<PostUIModel>>
    lateinit var navigateToPostTestSub: TestObserver<PostUIModel>
    lateinit var errorTestSub: TestObserver<String>


    describe("onLoadPosts") {
        beforeEachTest {
            Mockito.clearInvocations(postsViewModelSpy)
            refreshTestSub = postsViewModelSpy.outputs.refreshing().test()
            getPostsTestSub = postsViewModelSpy.outputs.getPosts().test()
            errorTestSub = postsViewModelSpy.outputs.error().test()
        }

        on("posts successfully fetched") {
            postsUseCaseMock.apply {
                Mockito.`when`(getPosts())
                    .thenReturn(Single.just(testPostsList))
            }

            postsViewModelSpy.onLoadPosts()

            it("emits refreshing start and end") {
                refreshTestSub.assertValueAt(1, true)
                refreshTestSub.assertValueAt(2, false)
            }

            it("emits no errors") {
                errorTestSub.assertNoValues()
            }

            it("emits posts") {
                getPostsTestSub.assertValue(testPostsList)
            }

        }

        on("error fetching posts") {
            postsUseCaseMock.apply {
                Mockito.`when`(getPosts())
                    .thenReturn(Single.error(Throwable("error")))
            }

            postsViewModelSpy.onLoadPosts()

            it("emits refreshing start and end") {
                refreshTestSub.assertValueAt(1, true)
                refreshTestSub.assertValueAt(2, false)
            }

            it("emits error") {
                errorTestSub.assertValue("Error loading posts: error")
            }

            it("emits no posts") {
                getPostsTestSub.assertNoValues()
            }

        }
    }

    describe("onPostClicked") {
        beforeEachTest {
            Mockito.clearInvocations(postsViewModelSpy)
            navigateToPostTestSub = postsViewModelSpy.outputs.navigateToPost().test()
            errorTestSub = postsViewModelSpy.outputs.error().test()
        }
        on("post position:$testPosition clicked") {

            postsViewModelSpy.onPostClicked(testPosition)

            it("emits post to navigate to") {
                navigateToPostTestSub.assertValue(testPost)
            }
        }
    }

})